package com.ruoyi.thread;

import com.ruoyi.netty.client.NettyClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: [青衫] 'QSSSYH@QQ.com'
 * @Date: 2019-07-31 21:03
 * @Description: < 描述 >
 */
@Component
public class ThreadCliennt implements Runnable
	{

		@Autowired
		private NettyClient nettyClient;
		/**
		 *
		 */
		@Override
		public void run()
			{
				nettyClient.serverRun();
			}
	}
