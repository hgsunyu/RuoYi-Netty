package com.ruoyi;

import cn.hutool.core.thread.ThreadUtil;
import com.ruoyi.netty.server.NettyServer;
import com.ruoyi.thread.ThreadCliennt;
import com.ruoyi.thread.ThreadServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RuoYiApplication
	{

		private static ThreadCliennt threadCliennt;

		private static ThreadServer threadServer;

		@Autowired
		private void setThreadCliennt(ThreadCliennt threadCliennt)
			{
				RuoYiApplication.threadCliennt = threadCliennt;

			}

		@Autowired
		private void setThreadServer(ThreadServer threadServer)
			{
				RuoYiApplication.threadServer = threadServer;

			}

		public static void main(String[] args)
			{


				// System.setProperty("spring.devtools.restart.enabled", "false");8
				SpringApplication.run(RuoYiApplication.class, args);
				System.err.println("Web服务启动成功 ->>>>>>>>>>>>>>>>>>>>>>");

				// 在当前线程组中执行线程
				ThreadUtil.execute(threadCliennt);
				ThreadUtil.execute(threadServer);

			}


		/**
		 * netty服务端
		 */
		public static NettyServer nettyServer;

		@Autowired
		private void setNettyServer(NettyServer nettyServer)
			{
				RuoYiApplication.nettyServer = nettyServer;
			}


	}