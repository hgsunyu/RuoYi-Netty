## 平台简介

Fork自若依 基于若依框架提供了

> 如需单应用，请移步 [RuoYi-fast](https://gitee.com/y_project/RuoYi-fast)  `(保持同步更新)`，如需Oracle版本，请移步 [RuoYi-oracle](http://doc.ruoyi.vip/#/standard/xmkz)  `(不定时更新)`


## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10.  登录日志：系统登录日志记录查询包含登录异常。
11.  在线用户：当前系统中活跃用户状态监控。
12.  定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13.  代码生成：前后端代码的生成（java、html、xml、sql)支持CRUD下载 。
14.  系统接口：根据业务代码自动生成相关的api接口文档。
15.  服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16.  在线构建器：拖动表单元素生成相应的HTML代码。
17.  连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
18.  Netty客户端: Netty客户端这里主要是测试连接服务端的简单实现 后期会继承protobuf 重新实现
19.  Netty服务端: 服务端默认使用' $ '做为分隔符 进行解包 后期会实现自定义解包器的实现


## 模块结构

![模块结构图](C:/Users/Administrator/Desktop/11.png)

## protocol下载
https://github.com/protocolbuffers/protobuf/releases/tag/v3.9.0

***

# RuoYi-Netty简介:

## 目的

使开发人员在不使用自定义解码器的情况下无需对Netty有过多的了解,通过一些灵活简单的配置即可使用Netty

,减少学习成本 

## 想法

1. 通过Web后台可灵活配置Netty多端口 
2. 通过Web后台灵活配置Netty的解码器 编码器然后启用相应的的解码器
3. 之后的版本会集成protobuf 但还没想好怎么减少protobuf的学习成本 

## 0.0.1版本

1. 初步集成Netty 和Hutool 简单实现服务端和客户端之间的通讯 使用' $ '做为默认分隔符进行解包
2. 单端口监听 服务端单端口启动监听
3. 通过程序入口main()方法 主线程启动Web服务 子线程1启动Netty客户端服务 子线程2启动Netty服务端服务



