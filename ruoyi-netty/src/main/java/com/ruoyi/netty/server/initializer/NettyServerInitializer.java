package com.ruoyi.netty.server.initializer;

import com.ruoyi.netty.server.handler.NettyServerHandler;
import com.ruoyi.netty.server.pojo.NettyInit;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: [青衫] 'QSSSYH@QQ.com'
 * @Date: 2019-07-08 9:59
 * @Description: < 描述 >
 */
@Component
public class NettyServerInitializer extends ChannelInitializer<SocketChannel>
	{


		@Autowired
		private NettyServerHandler nettyServerHandler;

		@Autowired
		private NettyInit nettyInit;

		@Override
		protected void initChannel(SocketChannel ch) throws Exception
			{
				ChannelPipeline channelPipeline = ch.pipeline();

				// 分隔符解码器
				ByteBuf delimiter = Unpooled.copiedBuffer(nettyInit.getSeparator().getBytes());
				channelPipeline .addLast(new DelimiterBasedFrameDecoder(nettyInit.getDeCodeSize(), delimiter));
				// 字符串解码 和 编码
				channelPipeline .addLast(new StringDecoder());
				channelPipeline.addLast(new StringEncoder());


				// 处理器
				channelPipeline .addLast(nettyServerHandler);
			}
	}