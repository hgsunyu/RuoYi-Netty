package com.ruoyi.netty.server.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


/**
 * @author: [青衫] 'QSSSYH@QQ.com'
 * @Date: 2019-07-08 10:47
 * @Description: < 描述 >
 */
@ChannelHandler.Sharable
@Component
// 这里的泛型指的你能在当前Handler接收到什么类型的数据
public class NettyServerHandler extends SimpleChannelInboundHandler<Object>
	{



		@Override
		protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception
			{
				String body = (String) msg;
				try
					{
						System.out.println("解包后的数据" + body);
						//  TODO 执行拿到数据后的操作
						//  ......

						// 向客户端回写数据
						ctx.channel().writeAndFlush("我已接收到数据! 这条消息是我通知给你的" + LocalDateTime.now()+"$");
					} catch (Exception e)
					{
						e.printStackTrace();
					}
			}

		/**
		 * 出现异常进行的处理
		 *
		 * @param ctx
		 * @param cause
		 *
		 * @throws Exception
		 */
		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
			{
				System.out.println("出现异常停止运行...");
				cause.printStackTrace();
				ctx.close();
			}
	}
