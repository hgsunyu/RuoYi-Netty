package com.ruoyi.netty.server.pojo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: 封润
 * @Date: 2019-07-23 21:36
 * @Description: <初始化参数对象 >
 */
@Component
@Data
@ConfigurationProperties(prefix = "netty-init")
@PropertySource(value = { "classpath:application-netty.yml" })
public class NettyInit {
    /**
     * Netty可进行多端口监听 这里初步只做单端口监听
     */
    List<Integer> serverPortList;

    /**
     * 服务端监听端口
     */
    @Value("${server-port}")
    Integer serverPort;
    /**
     * 设置解码器最大长度 防止恶意数据
     */
    @Value("${de-code-size}")
    Integer deCodeSize;
    /**
     * 分隔符号
     */
    @Value("${separator}")
    String separator;
}
