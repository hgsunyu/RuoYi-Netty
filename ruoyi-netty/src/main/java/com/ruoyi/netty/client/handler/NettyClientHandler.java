package com.ruoyi.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author: [青衫] 'QSSSYH@QQ.com'
 * @Date: 2019-07-31 20:27
 * @Description: < 处理器 >
 */
@Component
@ChannelHandler.Sharable
public class NettyClientHandler extends SimpleChannelInboundHandler<Object>
	{
		/**
		 * 解码完成后调用当前处理器
		 *
		 * @param channelHandlerContext
		 * @param o                     接收到的数据
		 *
		 * @throws Exception
		 */
		@Override
		protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception
			{

				String msg = (String) o;
				// 处理服务端发送来的数据
				System.err.println("已经接收到服务端发送来的数据:" + o);
				// 向服务端回写数据 $是分隔符 解码器要用的
				channelHandlerContext.writeAndFlush("我已经接收到服务端发送的数据:" + UUID.randomUUID()+"$");

			}

		/**
		 * 客户端服务端建立连接后调用当前方法
		 *
		 * @param ctx 连接通道
		 *
		 * @throws Exception
		 */
		@Override
		public void channelActive(ChannelHandlerContext ctx) throws Exception
			{
				//ctx.writeAndFlush("服务端你好! 我已经与你建立连接"+"$");
			}

		@Override
		public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
			{
				cause.printStackTrace();
				ctx.close();
			}
	}
